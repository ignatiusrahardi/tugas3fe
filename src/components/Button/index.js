import React from 'react';
import './style.css';

class Button extends React.PureComponent {
    render(){
        return (
            <button
            className="btn-component-wrap"
            onClick={this.props.handleClick}>Upload</button>
        );
    }
}

export default Button;