import React from 'react';
import './Form.css';
import Button from '../components/Button/index';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

class Form extends React.PureComponent {

    constructor(props){
        super(props)
        this.state = {
            file: null,
            uploaded: false,
            progress: '',
            error: false,
        }
        this.sendData = this.sendData.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.WebSocketClient = this.WebSocketClient.bind(this);
        // this.on_message_display= this.on_message_display.bind(this);
    }

    onFormSubmit(e){
        e.preventDefault() // Stop form submit
        this.sendData(this.state.file)
    }
    
    sendData() {
        let key = Math.random().toString(36).substring(7);
        
        var data = new FormData()
        console.log("random ", key);
        data.append('thetype', 'zip')
        data.append('thefile', this.state.file)
        this.WebSocketClient(key)
        const endpoint = 'http://127.0.0.1:8000/compress/';
        fetch(endpoint, {
            method: 'POST',
            headers: {
                'X-ROUTING-KEY': key,
            },
            body: data
        }).then(
            success => {
                this.setState({uploaded:true})
                console.log(success)
             } // if the response is a JSON object
          ).catch(
            error => {
                this.setState({error:true})
                console.log(error)
            } // Handle the error response object
          );
    }

    onChange(e) {
        this.setState({file:e.target.files[0]})
    }
    // on_message_display = (m) => {
    //     console.log('message received');
    //     this.setState({progress: m.body});
    // }
    WebSocketClient(key) {

        if ("WebSocket" in window) {
            // this.percentage('10');
            var ws_stomp_display = new SockJS( 'http://152.118.148.95:15674/stomp');
            var client_display = Stomp.over(ws_stomp_display);
            client_display.reconnect_delay = 5000;
            console.log(key)
            var mq_queue_display = "/exchange/1606875951/" + key;
            var on_connect_display = function() {
                console.log('connected');
                client_display.subscribe(mq_queue_display, on_message_display);
            }.bind(this);
            var on_error_display = function() {
                console.log('error');
            };
            var on_message_display = function(m) {
                console.log('message received');
                this.setState({progress: m.body});
            }.bind(this);
            client_display.connect('0806444524', '0806444524', on_connect_display, on_error_display, '/0806444524');
            } else {
            // The browser doesn't support WebSocket
                alert("WebSocket NOT supported by your Browser!");
            }
    }
    render(){
        return (
            <div>
                <div className="title">
                    <h1>Simple File Compression App</h1>
                    <h2>Upload only one file</h2>
                </div>
                <form className="fileUpload" onSubmit={this.onFormSubmit}>
                    {
                    this.state.uploaded ? (
                        <div>
                            {
                                this.state.progress === '100' ? (
                                    <h4>Done!</h4>
                                ) : (
                                    <h4>Compressing</h4>
                                )
                            }
                            <progress value={this.state.progress} max="100"></progress>
                            <p>Progress ({this.state.progress}%)</p>
                        </div>
                    ) : (
                        <div>
                            <input type="file" id='inputfile' onChange={this.onChange}/>
                            <Button/>
                            {
                                this.state.error ? (
                                    <h4>Opps, something wrong</h4>
                                ) : (
                                    <div></div>
                                )
                            }
                        </div>
                    )}
                </form>
            </div>
        );
    }
}

export default Form;